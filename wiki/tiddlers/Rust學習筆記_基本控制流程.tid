created: 20180715223954779
creator: renyuneyun
modified: 20180806231043119
modifier: renyuneyun
tags: Rust學習筆記
title: Rust學習筆記/基本控制流程
type: text/vnd.tiddlywiki

\define hf()
<<from "https://doc.rust-lang.org/stable/book/second-edition/ch03-05-control-flow.html">>
\end

理所當然地，Rust提供了`if`等控制流程。

! `if`表達式

`if`後接一個`bool`類型的表達式（不需要括號），且`if`和`else`子句（均爲表達式）均需要大括號。特殊地，如要書寫else-if，則`else if`合併爲一個表達式。

```rust
... 
let m = if a < 5 {
  println!("a小於5");
  4
} else if a < 8 {
  println!("a小於8");
  7
} else {
  10
}
println!("m={}", m);
```

注意`if`各子句（表達式）的返回值應當是相同的類型，否則會造成編譯錯誤（類型錯誤）。

另外，`if`表達式還支持模式匹配，使用`if let`語法。詳見[[Rust學習筆記/enum和模式匹配]]。

! 循環結構

Rust支持三種循環結構`loop`、`while`和`for`。

!! `loop`

`loop`產生一個無限循環，等價於C中的`while (1)`。

```rust
fn main() {
    loop {
        println!("again!");
    }
}
```

<<hf>>

!! `while`

`while`就是正常的`while`結構，不進行贅述。

同`if`一樣，其條件表達式也不需要括號；同`if`一樣，其也支持`while let`語法進行模式匹配（見[[Rust學習筆記/enum和模式匹配]]）。

!! `for`

`for`就是for each。同Python中的寫法類似，Rust中也需要`for ... in ...`。

```rust
fn main() {
    let a = [10, 20, 30, 40, 50];

    for element in a.iter() {
        println!("the value is: {}", element);
    }
}
```

```rust
fn main() {
    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}
```

<<hf>>
